jQuery(document).ready(function ($) {
    loadData();
});

async function loadData(length) {
    var url = "https://slack.com/api/conversations.history";
    var token = "token=xoxp-795055256386-812447905477-851756994930-e0dfb01edfe5211778112ecf99aca2d5"
    var channel = "channel=CS624L7UP";
    var limit ="limit=8";
    var result;
    var data;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            result = JSON.parse(this.responseText);
            var messages = sortMessages(result.messages);
            console.log(messages);
            data = setData(messages);
            resetSlider();
            setSliderContent(data);
        }
    };
    xhttp.open("GET", url + "?" + token + "&" + channel + "&" + limit, true);
    xhttp.send();
}

function sortMessages(messages) {
    var localMessages = []
    messages.forEach(message => {
        if (!message.subtype) {
            localMessages.push(message);
        }
    });
    localMessages.reverse()
    return localMessages;
}

function resetSlider() {
    $( "ul" ).remove();
    $( "#slider" ).append("<ul></ul>");
}

function setData(messages) {
    var data = [];
    for (const message of messages) {
        data.push(
            {
                message: message.text,
                timestamp: parseInt(message.ts),
                imagePath: message.files ? message.files[0].url_private : "https://cdn.loesdau.de/oxid/shops/loesdau/de/live/productimages/1500x1500/54240-bucas-fliegendecke-freedom-fly-sheet-paradise-pink.jpg"
            }
        );
        message.files ? getImage(message.files[0].url_private) : null
    }
    return data;
}

async function getImage(url) {
    var url = url;
    var token = "xoxp-795055256386-812447905477-851756994930-e0dfb01edfe5211778112ecf99aca2d5";
    await fetch(url, {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
    })
 //    .then(response => {
 //        response.body.getReader().read().then(imgData => {
 //            var arrayBufferView = new Uint8Array(imgData.data);
 //            var blob = new Blob( [ arrayBufferView ], { type: "image/jpeg" } );
 //            var urlCreator = window.URL || window.webkitURL;
 //           var imageUrl = urlCreator.createObjectURL( blob );
 //        });
 //    });
}

function setSliderContent(data) {
    var interval = 5; // in Sekunden
    data.forEach(object => {
        $( "ul" ).append("<li><div class='grid'><left>"
            + (object.imagePath
                ? "<span class='helper'></span><img src='" + object.imagePath + "'>"
                : "")
            + "</left><right>"
            + object.message + " ("
            + new Date(object.timestamp * 1000).toLocaleDateString() + ")"
            + "</right></div></li>");
    });
    $( ".interval span" ).append("Interval in Sekunden: " + interval.toString());
    setSlider(interval * 1000, data.length);
}
